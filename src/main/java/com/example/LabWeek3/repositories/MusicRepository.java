package com.example.LabWeek3.repositories;

import com.example.LabWeek3.models.MusicModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface MusicRepository extends JpaRepository<MusicModel, UUID> {

    Optional<MusicModel> findByTitle(String title);
    List<MusicModel> findByArtist(String artist);
}
