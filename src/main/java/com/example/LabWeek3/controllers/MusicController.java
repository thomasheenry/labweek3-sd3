package com.example.LabWeek3.controllers;

import com.example.LabWeek3.dtos.MusicRecordDto;
import com.example.LabWeek3.models.MusicModel;
import com.example.LabWeek3.repositories.MusicRepository;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class MusicController {

    @Autowired
    MusicRepository musicRepository;

    @PostMapping("/musics")
    public ResponseEntity<MusicModel> saveMusic (@RequestBody @Valid MusicRecordDto musicRecordDto) {
        var musicModel = new MusicModel();
        BeanUtils.copyProperties(musicRecordDto, musicModel);
        return ResponseEntity.status(HttpStatus.CREATED).body(musicRepository.save(musicModel));
    }

    @GetMapping("/musics")
    public ResponseEntity<List<MusicModel>> getAllMusics(){
        return ResponseEntity.status(HttpStatus.OK).body(musicRepository.findAll());
    }

    @GetMapping("/musics/{id}")
    public ResponseEntity<Object> getOneMusic(@PathVariable(value="id") UUID id){
        Optional<MusicModel> productO = musicRepository.findById(id);
        if(productO.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");
        }
        return ResponseEntity.status(HttpStatus.OK).body(productO.get());
    }

    @GetMapping("/musics/title/{title}")
    public ResponseEntity<Object> getMusicByTitle(@PathVariable(value="title") String title){
        Optional<MusicModel> productO = musicRepository.findByTitle(title);
        if(productO.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Title not found");
        }
        return ResponseEntity.status(HttpStatus.OK).body(productO);
    }

    @GetMapping("/musics/artist/{artist}")
    public ResponseEntity<Object> getOneMusic(@PathVariable(value="artist") String artist){
        List<MusicModel> productO = musicRepository.findByArtist(artist);
        if(productO.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Artist not found");
        }
        return ResponseEntity.status(HttpStatus.OK).body(productO);
    }


    @PutMapping("/musics/{id}")
    public ResponseEntity<Object> updateMusic(@PathVariable(value="id") UUID id,
                                              @RequestBody @Valid MusicRecordDto musicRecordDto) {
        Optional<MusicModel> musicO = musicRepository.findById(id);
        if(musicO.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music not found");
        }
        var musicModel = musicO.get();
        BeanUtils.copyProperties(musicRecordDto, musicModel);
        return ResponseEntity.status(HttpStatus.OK).body(musicRepository.save(musicModel));
    }

    @DeleteMapping("/musics/{id}")
    public ResponseEntity<Object> deleteMusic(@PathVariable(value = "id") UUID id) {
        Optional<MusicModel> musicO = musicRepository.findById(id);
        if(musicO.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music not found");
        }
        musicRepository.delete(musicO.get());
        return ResponseEntity.status(HttpStatus.OK).body("Music deleted successfully");
    }
}
