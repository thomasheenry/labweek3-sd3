package com.example.LabWeek3.services;

import com.example.LabWeek3.models.MusicModel;
import com.example.LabWeek3.repositories.MusicRepository;

import java.util.List;
import java.util.Optional;

public class musicService {
    private final MusicRepository musicRepository;

    public musicService(MusicRepository musicRepository) {
        this.musicRepository = musicRepository;
    }

    public List<MusicModel> searchMusicByArtist(String artist) {
        return musicRepository.findByArtist(artist);
    }

    public Optional<MusicModel> searchMusicByTitle(String title) {
        return musicRepository.findByTitle(title);
    }
}
