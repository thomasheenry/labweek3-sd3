package com.example.LabWeek3.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.time.LocalTime;

public record MusicRecordDto(@NotBlank String title, @NotBlank String artist, @NotBlank String gender, @NotBlank String minutes) {
}
